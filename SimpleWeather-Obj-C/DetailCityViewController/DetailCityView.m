//
//  DetailCityView.m
//  SimpleWeather Obj-C
//
//  Created by Developer on 23.12.2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "DetailCityView.h"

@implementation DetailCityView

- (void)setWeatherModel:(Weather *)weatherModel {
    [self weatherTextForState:weatherModel.weatherId];
    
    self.descriptionLabel.attributedText = [self colorKeyWord];
    self.weatherImageView.image = [DataManager weatherImageForIconString:weatherModel.icon];
    self.windLabel.text = [NSString stringWithFormat:@"Wind: %@", weatherModel.wind];
    self.weatherHuminidityLabel.text = [NSString stringWithFormat:@"Humidity: %@%%", weatherModel.humidity];
    self.temperatureLabel.text = [NSString stringWithFormat:@"%i℃", [weatherModel.temp intValue], nil];
    
    if([weatherModel.temp intValue] < 10) {
        [self.temperatureLabel setTextColor:[UIColor blueColor]];
    } else if([weatherModel.temp intValue] > 20) {
        [self.temperatureLabel setTextColor:[UIColor redColor]];
    } else {
        [self.temperatureLabel setTextColor:[UIColor blackColor]];
    }
}

- (void)weatherTextForState:(NSNumber *)weatherState {
    if ([weatherState intValue] == 800) {
        self.titleLabel.text = @"Keep smiling!";
        self.descriptionLabel.text = @"It is sunny day";
    } else if ([weatherState intValue] == -800) {
        self.titleLabel.text = @"Beautiful night!";
        self.descriptionLabel.text = @"Sleep well";
    } else {
        int weatherClassId = [weatherState intValue]/100;
        
        switch (weatherClassId) {
            case 2:
                self.titleLabel.text = @"Watch out for lightning!";
                self.descriptionLabel.text = @"The storm is coming";
                break;
            case 3:
                self.titleLabel.text = @"Small drops from the sky.";
                self.descriptionLabel.text = @"Drizzle behind the window";
                break;
            case 5:
                self.titleLabel.text = @"Take an umbrella!";
                self.descriptionLabel.text = @"It's raining outside";
                break;
            case 6:
                self.titleLabel.text = @"Dress warmly!";
                self.descriptionLabel.text = @"Winter is coming";
                break;
            case 7:
                self.titleLabel.text = @"Be attentive!";
                self.descriptionLabel.text = @"Fog covered the world";
                break;
            case 8:
                self.titleLabel.text = @"Nothing special...";
                self.descriptionLabel.text = @"Cloudy day";
                break;
            case 9:
                self.titleLabel.text = @"Prepare for anything!";
                self.descriptionLabel.text = @"The weather is crazy";
                break;
            
            default:
                self.titleLabel.text = @"Check the window.";
                self.descriptionLabel.text = @"I do not know what's going on";
                break;
        }
    }
}

- (NSMutableAttributedString *)colorKeyWord {
    NSArray *keywords = [NSArray arrayWithObjects:@"sunny", @"storm", @"Drizzle", @"raining", @"Winter", @"Fog", @"Cloudy", @"weather", @"Sleep", nil];
    NSArray *colors = [NSArray arrayWithObjects:[UIColor colorWithRed:255.0/255.0 green:210.0/255.0 blue:64.0/255.0 alpha:1.0],
                       [UIColor colorWithRed:63.0/255.0 green:45.0/255.0 blue:194.0/255.0 alpha:1.0],
                       [UIColor colorWithRed:14.0/255.0 green:214.0/255.0 blue:255.0/255.0 alpha:1.0],
                       [UIColor colorWithRed:36.0/255.0 green:175.0/255.0 blue:255.0/255.0 alpha:1.0],
                       [UIColor colorWithRed:197.0/255.0 green:236.0/255.0 blue:255.0/255.0 alpha:1.0],
                       [UIColor colorWithRed:182.0/255.0 green:176.0/255.0 blue:191.0/255.0 alpha:1.0],
                       [UIColor colorWithRed:57.0/255.0 green:119.0/255.0 blue:191.0/255.0 alpha:1.0],
                       [UIColor colorWithRed:235.0/255.0 green:81.0/255.0 blue:110.0/255.0 alpha:1.0],
                       [UIColor colorWithRed:239.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1.0], nil];
    
    NSRange range = NSMakeRange(0, 0);
    UIColor *color = [UIColor blackColor];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.descriptionLabel.text];
    
    for(int i = 0; i < [keywords count]; i++) {
        if([self.descriptionLabel.text containsString:[keywords objectAtIndex:i]]) {
            range = [self.descriptionLabel.text rangeOfString:[keywords objectAtIndex:i]];
            color = [colors objectAtIndex:i];
        }
    }
    
    [attributedString addAttribute:NSForegroundColorAttributeName value:color range:range];
    
    return attributedString;
}

@end
