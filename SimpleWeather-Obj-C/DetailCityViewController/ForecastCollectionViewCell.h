//
//  ForecastCollectionViewCell.h
//  SimpleWeather Obj-C
//
//  Created by Developer on 27.12.2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ForecastModel.h"
#import "DataManager.h"

@interface ForecastCollectionViewCell : UICollectionViewCell

@property (nonatomic) Forecast *forecast;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *temperatureLabel;
@property (weak, nonatomic) IBOutlet UIImageView *weatherImageView;

@end
