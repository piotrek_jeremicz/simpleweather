//
//  ForecastCollectionViewCell.m
//  SimpleWeather Obj-C
//
//  Created by Developer on 27.12.2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "ForecastCollectionViewCell.h"

@implementation ForecastCollectionViewCell

- (void)setForecast:(Forecast *)forecast {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"MMM d\nHH:mm";
    
    [self.dateLabel setText:[formatter stringFromDate:forecast.date]];
    [self.temperatureLabel setText:[NSString stringWithFormat:@"%i℃", [forecast.temp intValue], nil]];
    [self.weatherImageView setImage:[DataManager weatherImageForIconString:forecast.icon]];
}

@end
