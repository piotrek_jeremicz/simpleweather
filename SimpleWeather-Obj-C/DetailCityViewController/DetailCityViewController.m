//
//  DetailCityViewController.m
//  SimpleWeather Obj-C
//
//  Created by Developer on 23.12.2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "DetailCityViewController.h"

@implementation DetailCityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:self.weatherModel.city.cityName];
    [self.detailCityView setWeatherModel:self.weatherModel];
    
    [[API manager] fiveDaysForecastForCity:self.weatherModel.city completion:^(NSArray *forecast, NSError *error) {
        self.forecastData = forecast;
        [self.detailCityView.forecastCollectionView reloadData];
    }];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.forecastData count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ForecastCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"forecastCell" forIndexPath:indexPath];
    [cell setForecast:[self.forecastData objectAtIndex:[indexPath row]]];
    
    return cell;
}

@end
