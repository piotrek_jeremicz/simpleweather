//
//  DetailCityViewController.h
//  SimpleWeather Obj-C
//
//  Created by Developer on 23.12.2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailCityView.h"
#import "WeatherModel.h"
#import "APIManager.h"
#import "ForecastCollectionViewCell.h"

@interface DetailCityViewController : UIViewController <UICollectionViewDataSource>

@property (strong, nonatomic) IBOutlet DetailCityView *detailCityView;

@property (nonatomic) Weather *weatherModel;
@property (nonatomic) NSArray *forecastData;

@end
