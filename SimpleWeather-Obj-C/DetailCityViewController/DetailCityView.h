//
//  DetailCityView.h
//  SimpleWeather Obj-C
//
//  Created by Developer on 23.12.2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WeatherModel.h"
#import "DataManager.h"

@interface DetailCityView : UIView

- (void)weatherTextForState:(NSNumber *)weatherState;
- (NSMutableAttributedString *)colorKeyWord;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *forecastCollectionView;

@property (weak, nonatomic) IBOutlet UILabel *windLabel;
@property (weak, nonatomic) IBOutlet UILabel *temperatureLabel;
@property (weak, nonatomic) IBOutlet UIImageView *weatherImageView;
@property (weak, nonatomic) IBOutlet UILabel *weatherHuminidityLabel;

@property (nonatomic) Weather *weatherModel;

@end
