//
//  DataManager.h
//  SimpleWeather Obj-C
//
//  Created by Developer on 20.12.2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CityModel.h"

@interface DataManager : NSObject

+ (DataManager *)sharedInstance;

- (void)addCity:(City *) cityModel;
- (void)removeCity:(City *) cityModel;

+ (UIImage *)weatherImageForIconString:(NSString *)string;

@property (nonatomic, readonly) NSArray* cities;
@property (nonatomic) NSMutableArray* citiesData;

@end
