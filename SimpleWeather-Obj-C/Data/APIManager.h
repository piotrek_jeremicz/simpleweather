//
//  APIManager.h
//  SimpleWeather Obj-C
//
//  Created by Developer on 23.12.2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "WeatherModel.h"
#import "ForecastModel.h"

@interface API : NSObject

+ (API *)manager;

- (void)send:(NSURL *)urlRequest completion:(void (^)(NSDictionary* response, NSError *error))block;

- (void)todayForecastForCity:(City *)city completion:(void (^)(Weather* weather, NSError *error))block;
- (void)fiveDaysForecastForCity:(City *)city completion:(void (^)(NSArray* forecast, NSError *error))block;

@end
