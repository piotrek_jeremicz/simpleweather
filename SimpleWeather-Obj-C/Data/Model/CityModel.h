//
//  CityModel.h
//  SimpleWeather Obj-C
//
//  Created by Developer on 20.12.2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface City: NSObject

- (id)initWithData:(NSDictionary*) data;
- (NSDictionary *)rawData;

@property (nonatomic) NSString* cityName;
@property (nonatomic) NSNumber *latitude;
@property (nonatomic) NSNumber *longitude;

@end
