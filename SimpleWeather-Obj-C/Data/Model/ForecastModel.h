//
//  ForecastModel.h
//  SimpleWeather Obj-C
//
//  Created by Developer on 27.12.2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Forecast : NSObject

- (id)initWithData:(NSDictionary*) data;

@property (nonatomic) NSDate *date;
@property (nonatomic) NSString *icon;
@property (nonatomic) NSNumber *temp;

@end
