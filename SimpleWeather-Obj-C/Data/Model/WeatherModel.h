//
//  WeatherModel.h
//  SimpleWeather Obj-C
//
//  Created by Developer on 23.12.2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "CityModel.h"

@interface Weather : NSObject

- (id)initForCity:(City*) city withData:(NSDictionary*) data;

@property (nonatomic) City *city;
@property (nonatomic) NSNumber *weatherId;
@property (nonatomic) CLLocationCoordinate2D coord;
@property (nonatomic) NSString *icon;
@property (nonatomic) NSNumber *temp;
@property (nonatomic) NSString *wind;
@property (nonatomic) NSNumber *humidity;

@end
