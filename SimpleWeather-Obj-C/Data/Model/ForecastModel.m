//
//  ForecastModel.m
//  SimpleWeather Obj-C
//
//  Created by Developer on 27.12.2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "ForecastModel.h"

@implementation Forecast

- (id)initWithData:(NSDictionary*) data {
    self = [super init];
    
    NSDictionary *mainDictionary = [data objectForKey:@"main"];
    NSNumber *temperature = (NSNumber *)[mainDictionary objectForKey:@"temp"];
    
    NSDictionary *weatherDictionary = [[data objectForKey:@"weather"] firstObject];
    NSString *icon = (NSString *)[weatherDictionary objectForKey:@"icon"];
    
    NSNumber *timestamp = (NSNumber *)[data objectForKey:@"dt"];
    NSDate *day = [NSDate dateWithTimeIntervalSince1970:[timestamp intValue]];
    
    [self setDate:day];
    [self setIcon:icon];
    [self setTemp:temperature];
    
    return self;
}

@end
