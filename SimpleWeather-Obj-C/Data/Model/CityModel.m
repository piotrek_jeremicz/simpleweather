//
//  CityModel.m
//  SimpleWeather Obj-C
//
//  Created by Developer on 20.12.2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CityModel.h"

@implementation City 
- (id)initWithData:(NSDictionary*) data {
    self = [super init];

    [self setCityName:[data objectForKey:@"city"]];
    [self setLatitude:[data objectForKey:@"latitude"]];
    [self setLongitude:[data objectForKey:@"longitude"]];
    
    return self;
}

- (NSDictionary *)rawData {
    return [NSDictionary dictionaryWithObjectsAndKeys: _cityName, @"city", self.latitude, @"latitude", self.longitude, @"longitude", nil];
}
@end
