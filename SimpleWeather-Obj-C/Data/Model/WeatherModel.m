//
//  WeatherModel.m
//  SimpleWeather Obj-C
//
//  Created by Developer on 23.12.2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "WeatherModel.h"

@implementation Weather

- (id)initForCity:(City*) city withData:(NSDictionary*) data {
    self = [super init];
    
    [self setCity:city];
    
    NSDictionary *coordDictionary = [data objectForKey:@"coord"];
    NSNumber *longitude = [coordDictionary objectForKey:@"lon"];
    NSNumber *latitude = [coordDictionary objectForKey:@"lat"];
    
    NSDictionary *mainDictionary = [data objectForKey:@"main"];
    NSNumber *humidity = (NSNumber *)[mainDictionary objectForKey:@"humidity"];
    NSNumber *temperature = (NSNumber *)[mainDictionary objectForKey:@"temp"];
    
    NSDictionary *weatherDictionary = [[data objectForKey:@"weather"] firstObject];
    NSString *icon = (NSString *)[weatherDictionary objectForKey:@"icon"];
    NSNumber *weatherNumber = (NSNumber *)[weatherDictionary objectForKey:@"id"];
    
    NSDictionary *windDictionary = [data objectForKey:@"wind"];
    NSNumber *deg = (NSNumber *)[windDictionary objectForKey:@"deg"];
    
    NSString *direction = @"";
    
    if([icon containsString:@"n"] && [weatherNumber intValue] == 800){
        weatherNumber = @(- weatherNumber.intValue);
    }
    
    if([deg floatValue] < 22.5) {
        direction = @"N";
    } else if([deg floatValue] < 67.5) {
        direction = @"NE";
    } else if([deg floatValue] < 112.5) {
        direction = @"E";
    } else if([deg floatValue] < 157.5) {
        direction = @"SE";
    } else if([deg floatValue] < 202.5) {
        direction = @"S";
    } else if([deg floatValue] < 247.5) {
        direction = @"SW";
    } else if([deg floatValue] < 292.5) {
        direction = @"W";
    } else if([deg floatValue] < 337.5) {
        direction = @"NW";
    } else {
        direction = @"N";
    }
    
    [self setIcon:icon];
    [self setWind:direction];
    [self setTemp:temperature];
    [self setHumidity:humidity];
    [self setWeatherId:weatherNumber];
    [self setCoord:CLLocationCoordinate2DMake([longitude floatValue], [latitude floatValue])];
    
    return self;
}

@end
