//
//  DataManager.m
//  SimpleWeather Obj-C
//
//  Created by Developer on 20.12.2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager

@synthesize citiesData = _citiesData;

+ (DataManager *)sharedInstance {
    static DataManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[DataManager alloc] init];
    });
    
    return sharedInstance;
}

- (NSArray *) cities {
    NSMutableArray *citiesArray = [[NSMutableArray alloc] init];
    
    for(NSDictionary *city in self.citiesData) {
        City *cityModel = [[City alloc] initWithData:city];
        
        [citiesArray addObject:cityModel];
    }
    
    return [NSArray arrayWithArray:citiesArray];
}

- (NSMutableArray *) citiesData {
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"citiesData"] == nil) {
        return [[NSMutableArray alloc] init];
    }
    
    return [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"citiesData"]];
}

- (void) setCitiesData:(NSMutableArray *)citiesData {
    [[NSUserDefaults standardUserDefaults] setObject:citiesData forKey:@"citiesData"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)addCity:(City *) cityModel {
    NSMutableArray *citiesArray = self.citiesData;
    [citiesArray addObject:[cityModel rawData]];
    [self setCitiesData: citiesArray];
}

- (void)removeCity:(City *) cityModel {
    NSMutableArray *citiesArray = self.citiesData;
    [citiesArray removeObject:[cityModel rawData]];
    [self setCitiesData:citiesArray];
}

+ (UIImage *)weatherImageForIconString:(NSString *)string {
    UIImage *icon = [UIImage imageNamed:string];
    
    if(icon == nil) {
        NSString *dayIcon = [string stringByReplacingOccurrencesOfString:@"n" withString:@"d"];
        UIImage *dayIconImage = [UIImage imageNamed:dayIcon];
        
        if(dayIconImage == nil) {
            return [UIImage imageNamed:@"na"];
        } else {
            return dayIconImage;
        }
    } else {
        return icon;
    }
}

@end
