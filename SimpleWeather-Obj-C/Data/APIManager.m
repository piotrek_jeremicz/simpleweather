//
//  APIManager.m
//  SimpleWeather Obj-C
//
//  Created by Developer on 23.12.2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "APIManager.h"

@implementation API

NSString *apiKey = @"aabee1178c1a738c01b9e03b9517d029";
NSString *serverPath = @"http://api.openweathermap.org/data/2.5/";

+ (API *)manager {
    static API *manager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        manager = [[API alloc] init];
    });
    
    return manager;
}

- (void)send:(NSURL *)urlRequest completion:(void (^)(NSDictionary* response, NSError *error))block {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:urlRequest];
    request.HTTPMethod = @"GET";
    
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if(error != nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                block(nil, error);
                return;
            });
        }
        
        NSError *jsonError;
        NSDictionary *resultDictionary = [NSJSONSerialization JSONObjectWithData:data options:NULL error:&jsonError];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            block(resultDictionary, nil);
            return;
        });
    }];
    [task resume];
}

- (void)todayForecastForCity:(City *)city completion:(void (^)(Weather* weather, NSError *error))block {
    NSString *parameters = [NSString stringWithFormat:@"weather?lat=%f&lon=%f&appid=%@&units=%@", [city.latitude floatValue], [city.longitude floatValue], apiKey, @"metric", nil];
    NSURL *sendURL = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@", serverPath, parameters, nil]];
    
    [self send:sendURL completion:^(NSDictionary *response, NSError *error) {
        Weather *weather = [[Weather alloc] initForCity:city withData:response];
        block(weather, error);
    }];
}

- (void)fiveDaysForecastForCity:(City *)city completion:(void (^)(NSArray* forecast, NSError *error))block {
    NSString *parameters = [NSString stringWithFormat:@"forecast?lat=%f&lon=%f&appid=%@&units=%@", [city.latitude floatValue], [city.longitude floatValue], apiKey, @"metric", nil];
    NSURL *sendURL = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@", serverPath, parameters, nil]];
    
    [self send:sendURL completion:^(NSDictionary *response, NSError *error) {
        
        NSArray *list = [response objectForKey:@"list"];
        NSMutableArray *array = [[NSMutableArray alloc] init];
        
        for(NSDictionary *dict in list) {
            Forecast *forecast = [[Forecast alloc] initWithData:dict];
            [array addObject:forecast];
        }
        
        block([NSArray arrayWithArray:array], error);
    }];
}

@end
