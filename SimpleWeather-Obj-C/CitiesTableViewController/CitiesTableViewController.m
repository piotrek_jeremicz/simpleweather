//
//  CitiesTableViewController.m
//  SimpleWeather Obj-C
//
//  Created by Developer on 20.12.2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "CitiesTableViewController.h"

@implementation CitiesTableViewController 

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.citiesData = [[DataManager sharedInstance] cities];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqual: @"addSegue"]) {
        UINavigationController *navigationController = segue.destinationViewController;
        AddCityViewController *controller = (AddCityViewController *)navigationController.topViewController;
        controller.delegate = self;
    } else if([segue.identifier isEqual: @"detailSegue"]) {
        DetailCityViewController *controller = segue.destinationViewController;
        Weather *model = sender;
        
        if(model != nil) {
            controller.weatherModel = model;
        }
    }
}

#pragma mark - Actions

- (IBAction)addButonAction:(UIBarButtonItem *)sender {
    [self performSegueWithIdentifier:@"addSegue" sender:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.citiesData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cityCell" forIndexPath:indexPath];
    [cell setModel:[self.citiesData objectAtIndex:[indexPath row]]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CityTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    [self performSegueWithIdentifier:@"detailSegue" sender:cell.weather];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(editingStyle == UITableViewCellEditingStyleDelete) {
        CityTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        [[DataManager sharedInstance] removeCity:cell.model];
        self.citiesData = [[DataManager sharedInstance] cities];
        
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - Add City View Controller Delegate

- (void)doneButtonWasPressed:(CLPlacemark *)data {    
    NSDictionary *cityData = [[NSDictionary alloc] initWithObjectsAndKeys:(NSString *)data.locality, @"city", [NSNumber numberWithDouble: (double)data.location.coordinate.latitude],  @"latitude", [NSNumber numberWithDouble: (double)data.location.coordinate.longitude], @"longitude", nil];
    City *city = [[City alloc] initWithData:cityData];
    
    for(City *currentCity in self.citiesData) {
        if(currentCity.cityName == city.cityName) {
            return;
        }
    }
    
    [[DataManager sharedInstance] addCity:city];
    
    self.citiesData = [[DataManager sharedInstance] cities];
    [self.tableView reloadData];
    
    [[API manager] todayForecastForCity:city completion:^(Weather *weather, NSError *error) {
        [self performSegueWithIdentifier:@"detailSegue" sender:weather];
    }];
}

@end
