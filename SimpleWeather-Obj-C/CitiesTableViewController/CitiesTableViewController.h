//
//  CitiesTableViewController.h
//  SimpleWeather Obj-C
//
//  Created by Developer on 20.12.2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"

#import "CityTableViewCell.h"
#import "DetailCityViewController.h"
#import "AddCityViewController.h"

@interface CitiesTableViewController : UITableViewController <AddCityViewControllerDelegate>

@property (nonatomic, strong) NSArray* citiesData;

@end
