//
//  CityTableViewCell.m
//  SimpleWeather Obj-C
//
//  Created by Developer on 20.12.2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "CityTableViewCell.h"

@implementation CityTableViewCell

- (void)setModel:(City *)cityModel {
    
    _model = cityModel;
    
    self.latitude = [self.model latitude];
    self.longitude = [self.model longitude];
    self.cityLabel.text = [self.model cityName];
    
    [[API manager] todayForecastForCity:self.model completion:^(Weather *weather, NSError *error) {
        self.weather = weather;
        self.temperatureLabel.text = [NSString stringWithFormat:@"%i℃", [weather.temp intValue], nil];
        
        if([weather.temp intValue] < 10) {
            [self.temperatureLabel setTextColor:[UIColor blueColor]];
        } else if([weather.temp intValue] > 20) {
            [self.temperatureLabel setTextColor:[UIColor redColor]];
        } else {
            [self.temperatureLabel setTextColor:[UIColor blackColor]];
        }
        
        self.weatherImageView.image = [DataManager weatherImageForIconString:weather.icon];
    }];
}

@end
