//
//  CityTableViewCell.h
//  SimpleWeather Obj-C
//
//  Created by Developer on 20.12.2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CityModel.h"
#import "APIManager.h"
#import "DataManager.h"

@interface CityTableViewCell : UITableViewCell

@property (nonatomic) City *model;
@property (nonatomic) Weather *weather;
@property (nonatomic, strong) NSNumber *latitude;
@property (nonatomic, strong) NSNumber *longitude;

@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *temperatureLabel;
@property (weak, nonatomic) IBOutlet UIImageView *weatherImageView;

@end
