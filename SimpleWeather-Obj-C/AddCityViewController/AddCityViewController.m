//
//  AddCityViewController.m
//  SimpleWeather Obj-C
//
//  Created by Developer on 20.12.2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "AddCityViewController.h"

@implementation AddCityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.addCityView.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self.doneBarButtonItem setEnabled:NO];
    
    UILongPressGestureRecognizer *longGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longGestureRecognizerAction:)];
    [longGestureRecognizer setMinimumPressDuration:0.5];
    
    [self.addCityView.mapView addGestureRecognizer:longGestureRecognizer];
    [self.addCityView.searchBar becomeFirstResponder];
}

#pragma mark - Actions

- (IBAction)doneButtonAction:(UIBarButtonItem *)sender {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    CLLocation *location = [[CLLocation alloc] initWithLatitude:self.addCityView.pinCoordinate.latitude longitude:self.addCityView.pinCoordinate.longitude];
    
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        if (error != nil) {
            return;
        }
        
        CLPlacemark *placemark = placemarks.firstObject;
        
        [self dismissViewControllerAnimated:YES completion:^{
            [self.delegate doneButtonWasPressed:placemark];
        }];
    }];
}

- (IBAction)cancelButtonAction:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)longGestureRecognizerAction:(UILongPressGestureRecognizer *)sender {
    if (sender.state != UIGestureRecognizerStateBegan) {
        return;
    }
    
    CGPoint touchPoint = [sender locationInView:self.addCityView.mapView];
    [self.addCityView setPinPoint:touchPoint];
    [self.doneBarButtonItem setEnabled:YES];
}

#pragma mark - Functions
- (NSArray *)createCityArray:(NSArray *)citiesArray {
    NSMutableArray *citiesSearchResult = [[NSMutableArray alloc] init];
    
    for (MKMapItem *cityItem in citiesArray) {
        if (cityItem.placemark.locality != nil && [self checkIfCity: cityItem existIn: citiesSearchResult]) {
            [citiesSearchResult addObject:cityItem];
        }
    }
    
    return [NSArray arrayWithArray:citiesSearchResult];
}

- (BOOL)checkIfCity:(MKMapItem *)cityItem existIn:(NSArray *)cityArray {
    for (MKMapItem *city in cityArray) {
        if (city.placemark.locality == cityItem.placemark.locality &&
            city.placemark.administrativeArea == cityItem.placemark.administrativeArea &&
            city.placemark.country == cityItem.placemark.country) {
            
            return NO;
        }
    }
    return YES;
}

#pragma mark - Search Bar Delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
    [request setNaturalLanguageQuery: searchText];
    [locationSearch cancel];
    
    NSError *error;
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:@"[^a-zA-Z ]" options:0 error:&error];
    NSArray* matches = [regex matchesInString:searchText options:0 range:NSMakeRange(0, [searchText length])];
    
    if(matches.count > 0) {
        [searchBar resignFirstResponder];
        
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Please use only alphabet letters." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        locationSearch = [[MKLocalSearch alloc] initWithRequest: request];
        [locationSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error) {
            cityMapItems = [self createCityArray:response.mapItems];
            [self.addCityView.tableView reloadData];
        }];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

#pragma mark - Table view data source and delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [cityMapItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cityCell" forIndexPath:indexPath];
    
    MKMapItem *cityItem = [cityMapItems objectAtIndex:[indexPath row]];
    
    [cell.textLabel setText: cityItem.placemark.locality];
    [cell.detailTextLabel setText: [NSString stringWithFormat:@"%@, %@", cityItem.placemark.administrativeArea, cityItem.placemark.country]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    MKMapItem *item =(MKMapItem *)[cityMapItems objectAtIndex:[indexPath row]];
    self.addCityView.pinCoordinate = item.placemark.coordinate;
    
    [self.doneBarButtonItem setEnabled:TRUE];
}

@end
