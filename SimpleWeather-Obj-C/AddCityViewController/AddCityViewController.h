//
//  AddCityViewController.h
//  SimpleWeather Obj-C
//
//  Created by Developer on 20.12.2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "AddCityView.h"

@protocol AddCityViewControllerDelegate
- (void)doneButtonWasPressed:(CLPlacemark *)data;
@end

@interface AddCityViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate> {
    NSArray *cityMapItems;
    MKLocalSearch *locationSearch;
}

@property (nonatomic, weak) id <AddCityViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneBarButtonItem;
@property (weak, nonatomic) IBOutlet AddCityView *addCityView;

@end
