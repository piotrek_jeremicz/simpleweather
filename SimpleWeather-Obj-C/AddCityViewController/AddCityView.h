//
//  AddCityView.h
//  SimpleWeather Obj-C
//
//  Created by Developer on 23.12.2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface AddCityView : UIView

- (void)zoomMap:(CLLocationCoordinate2D)coordinate;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) CGPoint pinPoint;
@property (nonatomic) CLLocationCoordinate2D pinCoordinate;
@end
