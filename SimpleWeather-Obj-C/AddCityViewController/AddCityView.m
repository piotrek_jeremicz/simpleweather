//
//  AddCityView.m
//  SimpleWeather Obj-C
//
//  Created by Developer on 23.12.2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "AddCityView.h"

@implementation AddCityView

- (void)setPinPoint:(CGPoint)pinPoint {
    self.pinCoordinate = [self.mapView convertPoint:pinPoint toCoordinateFromView:self.mapView];
}

- (void)setPinCoordinate:(CLLocationCoordinate2D)pinCoordinate {
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    annotation.coordinate = pinCoordinate;
    _pinCoordinate = pinCoordinate;
    
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView addAnnotation:annotation];
    
    [self zoomMap:annotation.coordinate];
}

- (void)zoomMap:(CLLocationCoordinate2D)coordinate {
    MKCoordinateRegion region = MKCoordinateRegionMake(coordinate, MKCoordinateSpanMake(0.1, 0.1));
    [self.mapView setRegion:region];
    
    [self.searchBar resignFirstResponder];
}

@end
