//
//  AppDelegate.h
//  SimpleWeather Obj-C
//
//  Created by Developer on 18.12.2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

